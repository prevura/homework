const tabs = document.querySelectorAll('.tabs-title');
const tabsContentItems = document.querySelectorAll(".tabs-content-items")

tabs.forEach(elem => {

    elem.addEventListener('click', () => {
        let tabID = elem.getAttribute("data-tab");
        let currentTab = document.querySelector(tabID);

        tabs.forEach((e => { e.classList.remove('active'); }));
        tabsContentItems.forEach(elem => {
            elem.classList.remove('active'); });
        elem.classList.add('active');
        currentTab.classList.add('active');

    })
})
