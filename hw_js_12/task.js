// Опишіть своїми словами різницю між функціями setTimeout() та setInterval().

// setTimeout виконує дію через задану кількість часу один раз, а setInterval буде виконуватися до моменту, поки його не зупинять або не переповниться стек викликів.

// Що станеться, якщо у функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Ні, функція виконається настільки швидко, наскільки це можливо.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам не потрібен?

// Щоб не навантажувати систему та не переповнити стек виклику функцій.







const images = document.querySelectorAll(".image-to-show");
const stopOnCLickBtn = document.querySelector(".stop-btn");
const playOnClickBtn = document.querySelector(".play-btn");
let counter = 0;

function onButtonClick() {
    document.addEventListener("click", event => {
        if (event.target.classList === stopOnCLickBtn.classList) {
            clearTimeout(showImagesWithDelay);// || clearInterval(showImagesWithDelay);
        } else if (event.target.classList === playOnClickBtn.classList) {
            clearTimeout(showImagesWithDelay);// || clearInterval(showImagesWithDelay);
            showImages();
        }
    });

    let showImagesWithDelay;

    // function showImages() {
    //     showImagesWithDelay = setInterval(() => {
    //         counter++;
    //         if (counter !== 0) {
    //             images[counter - 1].hidden = true;
    //         }
    //
    //         if (counter / images.length === 1) {
    //             counter = 0;
    //         }
    //
    //         images[counter].hidden = false;
    //     }, 3000);
    // }
    function showImages() {
        showImagesWithDelay = setTimeout(function showImage()  {
            counter++;
            if (counter !== 0) {
                images[counter - 1].hidden = true;
            }

            if (counter / images.length === 1) {
                counter = 0;
            }

            images[counter].hidden = false;
            setTimeout(showImage,3000);
        }, 3000);
    }

    showImages()
}

onButtonClick();
