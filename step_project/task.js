const tabServices = document.querySelectorAll(".service-menu-tabs-title");
const tabsContent = document.querySelectorAll(".our-services-tabs-content-items");

const tabs = document.querySelector('.service-menu-tabs');

tabs.addEventListener("click", e => {

    tabsContent.forEach(item => {
        if (e.target.dataset.tab === item.id) {
            tabServices.forEach(item => item.classList.remove('pseudo'))
            e.target.classList.add('pseudo')
            tabsContent.forEach(item => item.classList.remove('active'))
            const activeContent = document.querySelector(`#${e.target.dataset.tab}`)
            activeContent.classList.add('active')
        }
    })

})

        






let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("demo");
    let captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
    captionText.innerHTML = dots[slideIndex-1].alt;
}